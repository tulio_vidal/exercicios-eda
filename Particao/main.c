/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: tulio
 *
 * Created on 30 de Abril de 2018, 23:09
 */

#include <stdio.h>
#include <stdlib.h>
#include "set_disjoint.h"

/*
 * 
 */
int main(int argc, char** argv) {
    DisjSet S;
    int i, j, k, Set1, Set2;

    Initialize(S);
    j = k = 1;
    while (k <= 8) {
        j = 1;
        while (j < 128) {
            Set1 = Find(j, S);
            Set2 = Find(j + k, S);
            SetUnion(S, Set1, Set2);
            j += 2 * k;
        }
        k *= 2;
    }
    i = 1;
    for (i = 1; i <= 128; i++) {
        Set1 = Find(i, S);
        printf("%d**", Set1);
    }
    printf("\n");
    return (EXIT_SUCCESS);
}

