/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: tulio
 *
 * Created on 30 de Abril de 2018, 23:09
 */

#include <stdio.h>
#include <stdlib.h>
#include "set_disjoint.h"

void Initialize(DisjSet S) {
    int i;

    for (i = 128; i > 0; i--)
        S[ i ] = 0;
}

void SetUnion(DisjSet S, SetType Root1, SetType Root2) {
    if (S[ Root2 ] < S[ Root1 ])
        S[ Root1 ] = Root2;
    else {
        if (S[ Root1 ] == S[ Root2 ])
            S[ Root1 ]--;
        S[ Root2 ] = Root1;
    }
}

SetType Find(ElementType X, DisjSet S) {
    if (S[ X ] <= 0)
        return X;
    else
        return S[ X ] = Find(S[ X ], S);
}
