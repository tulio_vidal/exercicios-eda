/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lista2.h
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 15:20
 */

typedef struct listaDuplamenteEncadeada ListaDuplamenteEncadeada;

ListaDuplamenteEncadeada* criar(void);
ListaDuplamenteEncadeada* inserir(ListaDuplamenteEncadeada* lista, int elemento);
void imprimir(ListaDuplamenteEncadeada* lista);
void imprimirRecursao(ListaDuplamenteEncadeada* lista);
void imprimirReversa(ListaDuplamenteEncadeada* lista);
int verificar(ListaDuplamenteEncadeada* lista);
int verificarIgualdade(ListaDuplamenteEncadeada* lista, ListaDuplamenteEncadeada* lista2);
int retornarTamanho(ListaDuplamenteEncadeada* lista);
ListaDuplamenteEncadeada* buscar(ListaDuplamenteEncadeada* lista, int elemento);
ListaDuplamenteEncadeada* remover(ListaDuplamenteEncadeada* lista, int elemento);
ListaDuplamenteEncadeada* removerRecursao(ListaDuplamenteEncadeada* lista, int elemento);
void liberar(ListaDuplamenteEncadeada* lista);