/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdlib.h>
#include <stdio.h>
#include "lista3.h"

/**
 * @struct listaDuplamenteEncadeada
 * Lista Duplamente Encadeada
 * { 
 *  Proximo, 
 *  Elemento
 * }
 */
struct listaDuplamenteEncadeada {
    ListaDuplamenteEncadeada* proximo;
    ListaDuplamenteEncadeada* anterior;
    int elemento;
};

typedef struct listaDuplamenteEncadeada ListaDuplamenteEncadeada;

ListaDuplamenteEncadeada* criar() {
    return NULL;
}

ListaDuplamenteEncadeada* inserir(ListaDuplamenteEncadeada* lista, int elemento) {
    ListaDuplamenteEncadeada* listaDuplamenteEncadeada = (ListaDuplamenteEncadeada*) malloc(sizeof (ListaDuplamenteEncadeada));
    listaDuplamenteEncadeada->elemento = elemento;
    listaDuplamenteEncadeada->proximo = lista;
    listaDuplamenteEncadeada->anterior = NULL;

    if (lista != NULL) {
        lista->anterior = listaDuplamenteEncadeada;
    }
    return listaDuplamenteEncadeada;
}

void imprimir(ListaDuplamenteEncadeada* lista) {
    for (ListaDuplamenteEncadeada* temp = lista; temp != NULL; temp = temp->proximo) {
        printf("valores: %d \n", temp->elemento);
    }
}

void imprimirReversa(ListaDuplamenteEncadeada* lista) {
    int x = 0;

    ListaDuplamenteEncadeada * listaAux[retornarTamanho(lista)];
    for (ListaDuplamenteEncadeada* temp = lista; temp != NULL; temp = temp->proximo, x++) {
        listaAux[x] = temp;
    }

    for (int z = x - 1; z >= 0; z--) {
        printf("valores inversos: %d \n", listaAux[z]->elemento);
    }
}

void imprimirRecursao(ListaDuplamenteEncadeada* lista) {
    if (!verificar(lista)) {
        printf("valor: %d\n", lista->elemento);
        imprimirRecursao(lista->proximo);
    }
}

int verificar(ListaDuplamenteEncadeada* lista) {
    return lista == NULL;
}

int verificarIgualdade(ListaDuplamenteEncadeada* list1, ListaDuplamenteEncadeada* list2) {
    ListaDuplamenteEncadeada* lista1 = list1;
    ListaDuplamenteEncadeada* lista2 = list2;
    for (; lista1 != NULL && lista2 != NULL; lista1 = lista1->proximo,
            lista2 = lista2->proximo) {
        if (lista1->elemento != lista2->elemento)
            return 0;
    }
    return (lista1 == lista2);
}

int retornarTamanho(ListaDuplamenteEncadeada* lista) {
    int x = 0;

    for (ListaDuplamenteEncadeada* temp = lista; temp != NULL; temp = temp->proximo) {
        x++;
    }
    return x;
}

ListaDuplamenteEncadeada* buscar(ListaDuplamenteEncadeada* lista, int elemento) {
    for (ListaDuplamenteEncadeada* proximo = lista; proximo != NULL; proximo = proximo->proximo) {
        if (proximo->elemento == elemento)
            return proximo;
    }
    return NULL;
}

ListaDuplamenteEncadeada* remover(ListaDuplamenteEncadeada* lista, int elemento) {
    if (buscar(lista, elemento) == NULL) {
        return lista;
    }

    ListaDuplamenteEncadeada* proximo = buscar(lista, elemento);

    if (lista != proximo) {
        proximo->anterior->proximo = proximo->proximo;
    } else {
        lista = proximo->proximo;
    }

    if (proximo->proximo != NULL) {
        proximo->proximo->anterior = proximo->anterior;
    }

    free(proximo);
    return lista;
}

ListaDuplamenteEncadeada* removerRecursao(ListaDuplamenteEncadeada* lista, int elemento) {
    if (!verificar(lista)) {
        if (lista->elemento != elemento) {
            lista->proximo = removerRecursao(lista->proximo, elemento);
        } else {
            ListaDuplamenteEncadeada* aux = lista;
            lista = lista->proximo;
            free(aux);
        }
    }
    return lista;
}

void liberar(ListaDuplamenteEncadeada* lista) {
    ListaDuplamenteEncadeada* temp = lista;
    while (temp != NULL) {
        ListaDuplamenteEncadeada* aux = temp->proximo;
        free(temp);
        temp = aux;
    }
}