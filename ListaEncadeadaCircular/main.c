/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 14:28
 */

#include <stdio.h>
#include <stdlib.h>
#include "lista4.h"

int main(void) {
    ListaEncadeadaCircular* lista = criar();
    lista = inserir(lista, 100);
    lista = inserir(lista, 200);
    lista = inserir(lista, 300);
    imprimir(lista);
    verificar(lista);
    lista = remover(lista, 100);
    lista = removerRecursao(lista, 300);
    lista = buscar(lista, 200);
    imprimir(lista);
    // imprimirRecursao(lista);
    return 0;
}