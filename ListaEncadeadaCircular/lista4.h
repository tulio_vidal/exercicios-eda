/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lista2.h
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 15:20
 */

typedef struct listaEncadeadaCircular ListaEncadeadaCircular;

ListaEncadeadaCircular* criar(void);
ListaEncadeadaCircular* inserir(ListaEncadeadaCircular* lista, int elemento);
void imprimir(ListaEncadeadaCircular* lista);
void imprimirRecursao(ListaEncadeadaCircular* lista);
void imprimirReversa(ListaEncadeadaCircular* lista);
int verificar(ListaEncadeadaCircular* lista);
int retornarTamanho(ListaEncadeadaCircular* lista);
ListaEncadeadaCircular* buscar(ListaEncadeadaCircular* lista, int elemento);
ListaEncadeadaCircular* remover(ListaEncadeadaCircular* lista, int elemento);
ListaEncadeadaCircular* removerRecursao(ListaEncadeadaCircular* lista, int elemento);
void liberar(ListaEncadeadaCircular* lista);