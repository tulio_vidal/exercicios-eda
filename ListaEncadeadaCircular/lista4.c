/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdlib.h>
#include <stdio.h>
#include "lista4.h"

/**
 * @struct listaEncadeadaCircular
 * Lista EncadeadaCircular
 * { 
 *  Proximo, 
 *  Elemento
 * }
 */
struct listaEncadeadaCircular {
    ListaEncadeadaCircular* proximo;
    int elemento;
};

typedef struct listaEncadeadaCircular ListaEncadeadaCircular;

ListaEncadeadaCircular* criar() {
    return NULL;
}

ListaEncadeadaCircular* inserir(ListaEncadeadaCircular* lista, int valor) {
    ListaEncadeadaCircular* listaEnc = (ListaEncadeadaCircular*) malloc(sizeof (ListaEncadeadaCircular));
    listaEnc->elemento = valor;
    listaEnc->proximo = lista;
    return listaEnc;
}

void imprimir(ListaEncadeadaCircular* lista) {
    if (verificar(lista)) {
        return;
    }
    ListaEncadeadaCircular* proximo = lista;
    do {
        printf("%d \n", proximo->elemento);
        proximo = proximo->proximo;
    } while (proximo != NULL);
}

void imprimirReversa(ListaEncadeadaCircular* lista) {
    int x = 0;

    ListaEncadeadaCircular * listaAux[retornarTamanho(lista)];
    for (ListaEncadeadaCircular* temp = lista; temp != NULL; temp = temp->proximo, x++) {
        listaAux[x] = temp;
    }

    for (int z = x - 1; z >= 0; z--) {
        printf("valores inversos: %d \n", listaAux[z]->elemento);
    }
}

void imprimirRecursao(ListaEncadeadaCircular* lista) {
    if (!verificar(lista)) {
        printf("valor: %d\n", lista->elemento);
        imprimirRecursao(lista->proximo);
    }
}

int verificar(ListaEncadeadaCircular* lista) {
    return lista == NULL;
}

int retornarTamanho(ListaEncadeadaCircular* lista) {
    int x = 0;

    for (ListaEncadeadaCircular* temp = lista; temp != NULL; temp = temp->proximo) {
        x++;
    }
    return x;
}

ListaEncadeadaCircular* buscar(ListaEncadeadaCircular* lista, int elemento) {
    for (ListaEncadeadaCircular* proximo = lista; proximo != NULL; proximo = proximo->proximo) {
        if (proximo->elemento == elemento)
            return proximo;
    }
    return NULL;
}

ListaEncadeadaCircular* remover(ListaEncadeadaCircular* lista, int elemento) {
    if (verificar(lista->proximo)) {
        return lista;
    }

    ListaEncadeadaCircular* anterior = NULL;
    ListaEncadeadaCircular* prox = lista;

    while ((prox != NULL) && (prox->elemento != elemento)) {
        anterior = prox;
        prox = prox->proximo;
    }

    if (verificar(anterior)) {
        lista = prox->proximo;
    } else {
        anterior->proximo = prox->proximo;
    }

    free(prox);
    return lista;
}

ListaEncadeadaCircular* removerRecursao(ListaEncadeadaCircular* lista, int elemento) {
    if (!verificar(lista)) {
        if (lista->elemento != elemento) {
            lista->proximo = removerRecursao(lista->proximo, elemento);
        } else {
            ListaEncadeadaCircular* aux = lista;
            lista = lista->proximo;
            free(aux);
        }
    }
    return lista;
}

void liberar(ListaEncadeadaCircular* lista) {
    ListaEncadeadaCircular* temp = lista;
    while (temp != NULL) {
        ListaEncadeadaCircular* aux = temp->proximo;
        free(temp);
        temp = aux;
    }
}