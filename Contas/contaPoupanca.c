/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdlib.h>
#include <stdio.h>
#include "contaPoupanca.h"

/**
 * @struct listaEncadeada
 * Lista Encadeada
 * { 
 *  Proximo, 
 *  Elemento
 * }
 */
struct contaPoupanca {
    ContaPoupanca* proximo;
    int elemento;
    int numero;
    float saldo;
};

typedef struct contaPoupanca ContaPoupanca;

ContaPoupanca* criar() {
    return NULL;
}

ContaPoupanca* inserir(ContaPoupanca* lista, int elemento) {
    ContaPoupanca* novo = (ContaPoupanca*) malloc(sizeof (ContaPoupanca));
    ContaPoupanca* anterior = NULL;
    ContaPoupanca* proximo = lista;

    novo->elemento = elemento;

    while (proximo != NULL && proximo->elemento < elemento) {
        anterior = proximo;
        proximo = proximo->proximo;
    }

    novo->proximo = ((anterior != NULL) ? anterior->proximo : lista);

    if (anterior != NULL) {
        anterior->proximo = novo;
    } else {
        lista = novo;
    }
    return lista;
}

void imprimir(ContaPoupanca* lista) {
    for (ContaPoupanca* temp = lista; temp != NULL; temp = temp->proximo) {
        printf("valores: %d \n", temp->elemento);
    }
}

void imprimirReversa(ContaPoupanca* lista) {
    int x = 0;

    ContaPoupanca * listaAux[retornarTamanho(lista)];
    for (ContaPoupanca* temp = lista; temp != NULL; temp = temp->proximo, x++) {
        listaAux[x] = temp;
    }

    for (int z = x - 1; z >= 0; z--) {
        printf("valores inversos: %d \n", listaAux[z]->elemento);
    }
}

void imprimirRecursao(ContaPoupanca* lista) {
    if (!verificar(lista)) {
        printf("valor: %d\n", lista->elemento);
        imprimirRecursao(lista->proximo);
    }
}

int verificar(ContaPoupanca* lista) {
    return lista == NULL;
}

int verificarIgualdade(ContaPoupanca* list1, ContaPoupanca* list2) {
    ContaPoupanca* lista1 = list1;
    ContaPoupanca* lista2 = list2;
    for (; lista1 != NULL && lista2 != NULL; lista1 = lista1->proximo,
            lista2 = lista2->proximo) {
        if (lista1->elemento != lista2->elemento)
            return 0;
    }
    return (lista1 == lista2);
}

int retornarTamanho(ContaPoupanca* lista) {
    int x = 0;

    for (ContaPoupanca* temp = lista; temp != NULL; temp = temp->proximo) {
        x++;
    }
    return x;
}

ContaPoupanca* buscar(ContaPoupanca* lista, int elemento) {
    for (ContaPoupanca* proximo = lista; proximo != NULL; proximo = proximo->proximo) {
        if (proximo->elemento == elemento)
            return proximo;
    }
    return NULL;
}

ContaPoupanca* remover(ContaPoupanca* lista, int elemento) {
    if (verificar(lista->proximo)) {
        return lista;
    }

    ContaPoupanca* anterior = NULL;
    ContaPoupanca* prox = lista;

    while ((prox != NULL) && (prox->elemento != elemento)) {
        anterior = prox;
        prox = prox->proximo;
    }

    if (verificar(anterior)) {
        lista = prox->proximo;
    } else {
        anterior->proximo = prox->proximo;
    }

    free(prox);
    return lista;
}

ContaPoupanca* removerRecursao(ContaPoupanca* lista, int elemento) {
    if (!verificar(lista)) {
        if (lista->elemento != elemento) {
            lista->proximo = removerRecursao(lista->proximo, elemento);
        } else {
            ContaPoupanca* aux = lista;
            lista = lista->proximo;
            free(aux);
        }
    }
    return lista;
}

void liberar(ContaPoupanca* lista) {
    ContaPoupanca* temp = lista;
    while (temp != NULL) {
        ContaPoupanca* aux = temp->proximo;
        free(temp);
        temp = aux;
    }
}