/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdlib.h>
#include <stdio.h>
#include "contaBancaria.h"

/**
 * @struct listaEncadeada
 * Lista Encadeada
 * { 
 *  Proximo, 
 *  Elemento
 * }
 */
struct contaBancaria {
    ContaBancaria* proximo;
    int elemento;
    int numero;
    float saldo;
};

typedef struct contaBancaria ContaBancaria;

ContaBancaria* criar(void) {
    return NULL;
}

ContaBancaria* inserir(ContaBancaria* lista, int elemento) {
    ContaBancaria* novo = (ContaBancaria*) malloc(sizeof (ContaBancaria));
    ContaBancaria* anterior = NULL;
    ContaBancaria* proximo = lista;

    novo->elemento = elemento;

    while (proximo != NULL && proximo->elemento < elemento) {
        anterior = proximo;
        proximo = proximo->proximo;
    }

    novo->proximo = ((anterior != NULL) ? anterior->proximo : lista);

    if (anterior != NULL) {
        anterior->proximo = novo;
    } else {
        lista = novo;
    }
    return lista;
}

void imprimir(ContaBancaria* lista) {
    for (ContaBancaria* temp = lista; temp != NULL; temp = temp->proximo) {
        printf("valores: %d \n", temp->elemento);
    }
}

void imprimirReversa(ContaBancaria* lista) {
    int x = 0;

    ContaBancaria * listaAux[retornarTamanho(lista)];
    for (ContaBancaria* temp = lista; temp != NULL; temp = temp->proximo, x++) {
        listaAux[x] = temp;
    }

    for (int z = x - 1; z >= 0; z--) {
        printf("valores inversos: %d \n", listaAux[z]->elemento);
    }
}

void imprimirRecursao(ContaBancaria* lista) {
    if (!verificar(lista)) {
        printf("valor: %d\n", lista->elemento);
        imprimirRecursao(lista->proximo);
    }
}

int verificar(ContaBancaria* lista) {
    return lista == NULL;
}

int verificarIgualdade(ContaBancaria* list1, ContaBancaria* list2) {
    ContaBancaria* lista1 = list1;
    ContaBancaria* lista2 = list2;
    for (; lista1 != NULL && lista2 != NULL; lista1 = lista1->proximo,
            lista2 = lista2->proximo) {
        if (lista1->elemento != lista2->elemento)
            return 0;
    }
    return (lista1 == lista2);
}

int retornarTamanho(ContaBancaria* lista) {
    int x = 0;

    for (ContaBancaria* temp = lista; temp != NULL; temp = temp->proximo) {
        x++;
    }
    return x;
}

ContaBancaria* buscar(ContaBancaria* lista, int elemento) {
    for (ContaBancaria* proximo = lista; proximo != NULL; proximo = proximo->proximo) {
        if (proximo->elemento == elemento)
            return proximo;
    }
    return NULL;
}

ContaBancaria* remover(ContaBancaria* lista, int elemento) {
    if (verificar(lista->proximo)) {
        return lista;
    }

    ContaBancaria* anterior = NULL;
    ContaBancaria* prox = lista;

    while ((prox != NULL) && (prox->elemento != elemento)) {
        anterior = prox;
        prox = prox->proximo;
    }

    if (verificar(anterior)) {
        lista = prox->proximo;
    } else {
        anterior->proximo = prox->proximo;
    }

    free(prox);
    return lista;
}

ContaBancaria* removerRecursao(ContaBancaria* lista, int elemento) {
    if (!verificar(lista)) {
        if (lista->elemento != elemento) {
            lista->proximo = removerRecursao(lista->proximo, elemento);
        } else {
            ContaBancaria* aux = lista;
            lista = lista->proximo;
            free(aux);
        }
    }
    return lista;
}

void liberar(ContaBancaria* lista) {
    ContaBancaria* temp = lista;
    while (temp != NULL) {
        ContaBancaria* aux = temp->proximo;
        free(temp);
        temp = aux;
    }
}