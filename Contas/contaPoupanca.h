/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lista2.h
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 15:20
 */

typedef struct contaPoupanca ContaPoupanca;

ContaPoupanca* criar(void);
ContaPoupanca* inserir(ContaPoupanca* lista, int elemento);
void imprimir(ContaPoupanca* lista);
void imprimirRecursao(ContaPoupanca* lista);
void imprimirReversa(ContaPoupanca* lista);
int verificar(ContaPoupanca* lista);
int verificarIgualdade(ContaPoupanca* lista, ContaPoupanca* lista2);
int retornarTamanho(ContaPoupanca* lista);
ContaPoupanca* buscar(ContaPoupanca* lista, int elemento);
ContaPoupanca* remover(ContaPoupanca* lista, int elemento);
ContaPoupanca* removerRecursao(ContaPoupanca* lista, int elemento);
void liberar(ContaPoupanca* lista);
ContaPoupanca* realizarCredito(ContaPoupanca* conta, float valor);
ContaPoupanca* realizarDebito(ContaPoupanca* conta, float valor);
float consultarSaldo(ContaPoupanca* conta);
ContaPoupanca* renderJuros(ContaPoupanca* conta);
void realizarTransferencia(ContaPoupanca* conta, ContaPoupanca* conta2, float valor);
void removerConta(ContaPoupanca* conta);
void imprimirTodas(ContaPoupanca* conta);
