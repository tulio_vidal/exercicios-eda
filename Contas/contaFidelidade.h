/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   contaFidelidade.h
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 15:20
 */

typedef struct contaFidelidade ContaFidelidade;

ContaFidelidade* criar(void);
ContaFidelidade* inserir(ContaFidelidade* lista, int elemento);
void imprimir(ContaFidelidade* lista);
void imprimirRecursao(ContaFidelidade* lista);
void imprimirReversa(ContaFidelidade* lista);
int verificar(ContaFidelidade* lista);
int verificarIgualdade(ContaFidelidade* lista, ContaFidelidade* lista2);
int retornarTamanho(ContaFidelidade* lista);
ContaFidelidade* buscar(ContaFidelidade* lista, int elemento);
ContaFidelidade* remover(ContaFidelidade* lista, int elemento);
ContaFidelidade* removerRecursao(ContaFidelidade* lista, int elemento);
void liberar(ContaFidelidade* lista);
ContaFidelidade* realizarCredito(ContaFidelidade* conta, float valor);
ContaFidelidade* realizarDebito(ContaFidelidade* conta, float valor);
float consultarSaldo(ContaFidelidade* conta);
float consultarBonus(ContaFidelidade* conta);
ContaFidelidade* renderBonus(ContaFidelidade* conta);
void realizarTransferencia(ContaFidelidade* conta, ContaFidelidade* conta2, float valor);
void removerConta(ContaFidelidade* conta);
void imprimirTodas(ContaFidelidade* conta);