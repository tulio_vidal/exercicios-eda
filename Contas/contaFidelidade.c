/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdlib.h>
#include <stdio.h>
#include "contaFidelidade.h"

/**
 * @struct listaEncadeada
 * Lista Encadeada
 * { 
 *  Proximo, 
 *  Elemento
 * }
 */
struct contaFidelidade {
    ContaFidelidade* proximo;
    int elemento;
    int numero;
    float saldo;
    float bonus;
};

typedef struct contaFidelidade ContaFidelidade;

ContaFidelidade* criar() {
    return NULL;
}

ContaFidelidade* inserir(ContaFidelidade* lista, int elemento) {
    ContaFidelidade* novo = (ContaFidelidade*) malloc(sizeof (ContaFidelidade));
    ContaFidelidade* anterior = NULL;
    ContaFidelidade* proximo = lista;

    novo->elemento = elemento;

    while (proximo != NULL && proximo->elemento < elemento) {
        anterior = proximo;
        proximo = proximo->proximo;
    }

    novo->proximo = ((anterior != NULL) ? anterior->proximo : lista);

    if (anterior != NULL) {
        anterior->proximo = novo;
    } else {
        lista = novo;
    }
    return lista;
}

void imprimir(ContaFidelidade* lista) {
    for (ContaFidelidade* temp = lista; temp != NULL; temp = temp->proximo) {
        printf("valores: %d \n", temp->elemento);
    }
}

void imprimirReversa(ContaFidelidade* lista) {
    int x = 0;

    ContaFidelidade * listaAux[retornarTamanho(lista)];
    for (ContaFidelidade* temp = lista; temp != NULL; temp = temp->proximo, x++) {
        listaAux[x] = temp;
    }

    for (int z = x - 1; z >= 0; z--) {
        printf("valores inversos: %d \n", listaAux[z]->elemento);
    }
}

void imprimirRecursao(ContaFidelidade* lista) {
    if (!verificar(lista)) {
        printf("valor: %d\n", lista->elemento);
        imprimirRecursao(lista->proximo);
    }
}

int verificar(ContaFidelidade* lista) {
    return lista == NULL;
}

int verificarIgualdade(ContaFidelidade* list1, ContaFidelidade* list2) {
    ContaFidelidade* lista1 = list1;
    ContaFidelidade* lista2 = list2;
    for (; lista1 != NULL && lista2 != NULL; lista1 = lista1->proximo,
            lista2 = lista2->proximo) {
        if (lista1->elemento != lista2->elemento)
            return 0;
    }
    return (lista1 == lista2);
}

int retornarTamanho(ContaFidelidade* lista) {
    int x = 0;

    for (ContaFidelidade* temp = lista; temp != NULL; temp = temp->proximo) {
        x++;
    }
    return x;
}

ContaFidelidade* buscar(ContaFidelidade* lista, int elemento) {
    for (ContaFidelidade* proximo = lista; proximo != NULL; proximo = proximo->proximo) {
        if (proximo->elemento == elemento)
            return proximo;
    }
    return NULL;
}

ContaFidelidade* remover(ContaFidelidade* lista, int elemento) {
    if (verificar(lista->proximo)) {
        return lista;
    }

    ContaFidelidade* anterior = NULL;
    ContaFidelidade* prox = lista;

    while ((prox != NULL) && (prox->elemento != elemento)) {
        anterior = prox;
        prox = prox->proximo;
    }

    if (verificar(anterior)) {
        lista = prox->proximo;
    } else {
        anterior->proximo = prox->proximo;
    }

    free(prox);
    return lista;
}

ContaFidelidade* removerRecursao(ContaFidelidade* lista, int elemento) {
    if (!verificar(lista)) {
        if (lista->elemento != elemento) {
            lista->proximo = removerRecursao(lista->proximo, elemento);
        } else {
            ContaFidelidade* aux = lista;
            lista = lista->proximo;
            free(aux);
        }
    }
    return lista;
}

void liberar(ContaFidelidade* lista) {
    ContaFidelidade* temp = lista;
    while (temp != NULL) {
        ContaFidelidade* aux = temp->proximo;
        free(temp);
        temp = aux;
    }
}

ContaFidelidade*  realizarCredito(ContaFidelidade* conta, float valor) {
    conta->saldo += valor;
    conta->bonus += (valor * 0.01);
    return conta;
}

ContaFidelidade* realizarDebito(ContaFidelidade* conta, float valor) {
    conta->saldo -= valor;
    return conta;
}

float consultarSaldo(ContaFidelidade * conta) {
    return conta->saldo;
}

float consultarBonus(ContaFidelidade * conta) {
    return conta->bonus;
}

ContaFidelidade* renderBonus(ContaFidelidade * conta) {
    conta->saldo += conta->bonus;
    conta->bonus = 0;
    return conta;
}

void realizarTransferencia(ContaFidelidade* conta, ContaFidelidade* conta2, float valor) {
    conta2->saldo += valor;
    conta->saldo -= valor;
}

void removerConta(ContaFidelidade * conta) {
    if (!verificar(conta)) {
        free(conta);
    }
}

void imprimirTodas(ContaFidelidade * conta) {
    imprimir(conta);
}
