/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lista2.h
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 15:20
 */

typedef struct contaBancaria ContaBancaria;
typedef struct contaPoupanca ContaPoupanca;
typedef struct contaFidelidade ContaFidelidade;

ContaBancaria* criar(void);
ContaBancaria* inserir(ContaBancaria* lista, int elemento);
void imprimir(ContaBancaria* lista);
void imprimirRecursao(ContaBancaria* lista);
void imprimirReversa(ContaBancaria* lista);
int verificar(ContaBancaria* lista);
int verificarIgualdade(ContaBancaria* lista, ContaBancaria* lista2);
int retornarTamanho(ContaBancaria* lista);
ContaBancaria* buscar(ContaBancaria* lista, int elemento);
ContaBancaria* remover(ContaBancaria* lista, int elemento);
ContaBancaria* removerRecursao(ContaBancaria* lista, int elemento);
void liberar(ContaBancaria* lista);
ContaBancaria* realizarCredito(ContaBancaria* conta, float valor);
ContaBancaria* realizarDebito(ContaBancaria* conta, float valor);
float consultarSaldo(ContaBancaria* conta);
void realizarTransferencia(ContaBancaria* conta, ContaBancaria* conta2, float valor);
void removerConta(ContaBancaria* conta);
void imprimirTodas(ContaBancaria* conta);

ContaPoupanca* criar(void);
ContaPoupanca* inserir(ContaPoupanca* lista, int elemento);
void imprimir(ContaPoupanca* lista);
void imprimirRecursao(ContaPoupanca* lista);
void imprimirReversa(ContaPoupanca* lista);
int verificar(ContaPoupanca* lista);
int verificarIgualdade(ContaPoupanca* lista, ContaPoupanca* lista2);
int retornarTamanho(ContaPoupanca* lista);
ContaPoupanca* buscar(ContaPoupanca* lista, int elemento);
ContaPoupanca* remover(ContaPoupanca* lista, int elemento);
ContaPoupanca* removerRecursao(ContaPoupanca* lista, int elemento);
void liberar(ContaPoupanca* lista);
ContaFidelidade* realizarCredito(ContaPoupanca* conta, float valor);
ContaFidelidade* realizarDebito(ContaPoupanca* conta, float valor);
float consultarSaldo(ContaPoupanca* conta);
ContaFidelidade* renderJuros(ContaPoupanca* conta);
void realizarTransferencia(ContaPoupanca* conta, ContaPoupanca* conta2, float valor);
void removerConta(ContaPoupanca* conta);
void imprimirTodas(ContaPoupanca* conta);

ContaFidelidade* criar(void);
ContaFidelidade* inserir(ContaFidelidade* lista, int elemento);
void imprimir(ContaFidelidade* lista);
void imprimirRecursao(ContaFidelidade* lista);
void imprimirReversa(ContaFidelidade* lista);
int verificar(ContaFidelidade* lista);
int verificarIgualdade(ContaFidelidade* lista, ContaFidelidade* lista2);
int retornarTamanho(ContaFidelidade* lista);
ContaFidelidade* buscar(ContaFidelidade* lista, int elemento);
ContaFidelidade* remover(ContaFidelidade* lista, int elemento);
ContaFidelidade* removerRecursao(ContaFidelidade* lista, int elemento);
void liberar(ContaFidelidade* lista);
ContaFidelidade* realizarCredito(ContaFidelidade* conta, float valor);
ContaFidelidade* realizarDebito(ContaFidelidade* conta, float valor);
float consultarSaldo(ContaFidelidade* conta);
float consultarBonus(ContaFidelidade* conta);
ContaFidelidade* renderBonus(ContaFidelidade* conta);
void realizarTransferencia(ContaFidelidade* conta, ContaFidelidade* conta2, float valor);
void removerConta(ContaFidelidade* conta);
void imprimirTodas(ContaFidelidade* conta);