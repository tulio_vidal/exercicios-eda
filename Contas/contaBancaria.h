/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   contaBancaria.h
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 15:20
 */

typedef struct contaBancaria ContaBancaria;

ContaBancaria* criar(void);
ContaBancaria* inserir(ContaBancaria* lista, int elemento);
void imprimir(ContaBancaria* lista);
void imprimirRecursao(ContaBancaria* lista);
void imprimirReversa(ContaBancaria* lista);
int verificar(ContaBancaria* lista);
int verificarIgualdade(ContaBancaria* lista, ContaBancaria* lista2);
int retornarTamanho(ContaBancaria* lista);
ContaBancaria* buscar(ContaBancaria* lista, int elemento);
ContaBancaria* remover(ContaBancaria* lista, int elemento);
ContaBancaria* removerRecursao(ContaBancaria* lista, int elemento);
void liberar(ContaBancaria* lista);
ContaBancaria* realizarCredito(ContaBancaria* conta, float valor);
ContaBancaria* realizarDebito(ContaBancaria* conta, float valor);
float consultarSaldo(ContaBancaria* conta);
void realizarTransferencia(ContaBancaria* conta, ContaBancaria* conta2, float valor);
void removerConta(ContaBancaria* conta);
void imprimirTodas(ContaBancaria* conta);
