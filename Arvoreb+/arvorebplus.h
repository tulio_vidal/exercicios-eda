/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   arvoreb+.h
 * Author: tulio
 *
 * Created on 14 de Maio de 2018, 20:15
 */

#define MAX_CHILD 5

typedef struct ArvoreBPlus {
	int eRaiz, eFolha;
	int numChave;
	int chave[MAX_CHILD];
	int pos[MAX_CHILD];
	void* filho[MAX_CHILD];
	struct ArvoreBPlus* pai;
	struct ArvoreBPlus* proximo;
	struct ArvoreBPlus* anterior;
} ArvoreBPlus;

void ArvoreBPlus_SetMaxFilhos(int);
void ArvoreBPlus_Iniciar();
void ArvoreBPlus_Destruir();
int ArvoreBPlus_Inserir(int chave, int pos, void* valor);
int ArvoreBPlus_GetTotalNos();
void ArvoreBPlus_Modificar(int chave, void* valor);
void ArvoreBPlus_Deletar(int chave);
void ArvoreBPlus_Consultar_Chave(int chave);
void ArvoreBPlus_Consultar_Alcance(int l, int r);