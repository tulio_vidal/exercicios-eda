/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdlib.h>
#include <stdio.h>
#include "arvorebplus.h"

struct ArvoreBPlus* Raiz;

int MaxNumFilhos = 20;
int TotalNos;

int NumeroConsulta;

ArvoreBPlus* Novo_NoArvoreBPlus() {
    struct ArvoreBPlus* p = (struct ArvoreBPlus*) malloc(sizeof (struct ArvoreBPlus));
    p->eRaiz = 0;
    p->eFolha = 0;
    p->numChave = 0;
    p->filho[0] = NULL;
    p->pai = NULL;
    p->proximo = NULL;
    p->anterior = NULL;
    TotalNos++;
    return p;
}

void Inserir(ArvoreBPlus*, int, int, void*);

void Split(ArvoreBPlus* Cur) {
    ArvoreBPlus* Temp = Novo_NoArvoreBPlus();
    ArvoreBPlus* ch;
    int Meio = MaxNumFilhos >> 1;
    Temp->eFolha = Cur->eFolha;
    Temp->numChave = MaxNumFilhos - Meio;
    int i;
    for (i = Meio; i < MaxNumFilhos; i++) {
        Temp->filho[i - Meio] = Cur->filho[i];
        Temp->chave[i - Meio] = Cur->chave[i];
        if (Temp->eFolha) {
            Temp->pos[i - Meio] = Cur->pos[i];
        } else {
            ch = (ArvoreBPlus*) Temp->filho[i - Meio];
            ch->pai = Temp;
        }
    }
    Cur->numChave = Meio;
    if (Cur->eRaiz) {
        Raiz = Novo_NoArvoreBPlus();
        Raiz->numChave = 2;
        Raiz->eRaiz = 1;
        Raiz->chave[0] = Cur->chave[0];
        Raiz->filho[0] = Cur;
        Raiz->chave[1] = Temp->chave[0];
        Raiz->filho[1] = Temp;
        Cur->pai = Temp->pai = Raiz;
        Cur->eRaiz = 0;
        if (Cur->eFolha) {
            Cur->proximo = Temp;
            Temp->anterior = Cur;
        }
    } else {
        Temp->pai = Cur->pai;
        Inserir(Cur->pai, Cur->chave[Meio], -1, (void*) Temp);
    }
}

void Inserir(ArvoreBPlus* Cur, int chave, int pos, void* valor) {
    int i, ins;
    if (chave < Cur->chave[0]) ins = 0;
    else {
        ins = Busca_Binaria(Cur, chave) + 1;
    }
    for (i = Cur->numChave; i > ins; i--) {
        Cur->chave[i] = Cur->chave[i - 1];
        Cur->filho[i] = Cur->filho[i - 1];
        if (Cur->eFolha) Cur->pos[i] = Cur->pos[i - 1];
    }
    Cur->numChave++;
    Cur->chave[ins] = chave;
    Cur->filho[ins] = valor;
    Cur->pos[ins] = pos;
    if (Cur->eFolha == 0) { // make links on leaves
        ArvoreBPlus* primeiroFilho = (ArvoreBPlus*) (Cur->filho[0]);
        if (primeiroFilho->eFolha == 1) {
            ArvoreBPlus* temp = (ArvoreBPlus*) (valor);
            if (ins > 0) {
                ArvoreBPlus* filhoAnterior;
                ArvoreBPlus* filhoSucessor;
                filhoAnterior = (ArvoreBPlus*) Cur->filho[ins - 1];
                filhoSucessor = filhoAnterior->proximo;
                filhoAnterior->proximo = temp;
                temp->proximo = filhoSucessor;
                temp->anterior = filhoAnterior;
                if (filhoSucessor != NULL) filhoSucessor->anterior = temp;
            } else {
                temp->proximo = Cur->filho[1];
            }
        }
    }
    if (Cur->numChave == MaxNumFilhos)
        Split(Cur);
}

int Busca_Binaria(ArvoreBPlus* Cur, int chave) {
    int l = 0, r = Cur->numChave;
    if (chave < Cur->chave[l]) return l;
    if (Cur->chave[r - 1] <= chave) return r - 1;
    while (l < r - 1) {
        int meio = (l + r) >> 1;
        if (Cur->chave[meio] > chave)
            r = meio;
        else
            l = meio;
    }
    return l;
}

void Redistribuir(ArvoreBPlus* Cur) {
    if (Cur->eRaiz) {
        if (Cur->numChave == 1 && !Cur->eFolha) {
            Raiz = Cur->filho[0];
            Raiz->eRaiz = 1;
            free(Cur);
        }
        return;
    }
    ArvoreBPlus* Pai = Cur->pai;
    ArvoreBPlus* filhoAnterior;
    ArvoreBPlus* filhoSucessor;
    ArvoreBPlus* temp;
    int indice = Busca_Binaria(Pai, Cur->chave[0]);
    if (indice + 1 < Pai->numChave) {
        filhoSucessor = Pai->filho[indice + 1];
        if ((filhoSucessor->numChave - 1) * 2 >= MaxNumFilhos) {
            Resort(Cur, filhoSucessor);
            Pai->chave[indice + 1] = filhoSucessor->chave[0];
            return;
        }
    }
    if (indice - 1 >= 0) {
        filhoAnterior = Pai->filho[indice - 1];
        if ((filhoAnterior->numChave - 1) * 2 >= MaxNumFilhos) {
            Resort(filhoAnterior, Cur);
            Pai->chave[indice] = Cur->chave[0];
            return;
        }
    }
    if (indice + 1 < Pai->numChave) {
        int i = 0;
        while (i < filhoSucessor->numChave) {
            Cur->chave[Cur->numChave] = filhoSucessor->chave[i];
            Cur->filho[Cur->numChave] = filhoSucessor->filho[i];
            if (Cur->eFolha) {
                Cur->pos[Cur->numChave] = filhoSucessor->pos[i];
            } else {
                temp = (ArvoreBPlus*) (filhoSucessor->filho[i]);
                temp->pai = Cur;
            }
            Cur->numChave++;
            i++;
        }
        Deletar(Pai, filhoSucessor->chave[0]);
        return;
    }
    if (indice - 1 >= 0) {
        int i = 0;
        while (i < Cur->numChave) {
            filhoAnterior->chave[filhoAnterior->numChave] = Cur->chave[i];
            filhoAnterior->filho[filhoAnterior->numChave] = Cur->filho[i];
            if (Cur->eFolha) {
                filhoAnterior->pos[filhoAnterior->numChave] = Cur->pos[i];
            } else {
                temp = (ArvoreBPlus*) (Cur->filho[i]);
                temp->pai = filhoAnterior;
            }
            filhoAnterior->numChave++;
            i++;
        }
        Deletar(Pai, Cur->chave[0]);
        return;
    }
}

void Resort(ArvoreBPlus* Esquerda, ArvoreBPlus* Direita) {
    int total = Esquerda->numChave + Direita->numChave;
    ArvoreBPlus* temp;
    if (Esquerda->numChave < Direita->numChave) {
        int leftSize = total >> 1;
        int i = 0, j = 0;
        while (Esquerda->numChave < leftSize) {
            Esquerda->chave[Esquerda->numChave] = Direita->chave[i];
            Esquerda->filho[Esquerda->numChave] = Direita->filho[i];
            if (Esquerda->eFolha) {
                Esquerda->pos[Esquerda->numChave] = Direita->pos[i];
            } else {
                temp = (ArvoreBPlus*) (Direita->filho[i]);
                temp->pai = Esquerda;
            }
            Esquerda->numChave++;
            i++;
        }
        while (i < Direita->numChave) {
            Direita->chave[j] = Direita->chave[i];
            Direita->filho[j] = Direita->filho[i];
            if (Direita->eFolha) Direita->pos[j] = Direita->pos[i];
            i++;
            j++;
        }
        Direita->numChave = j;
    } else {
        int leftSize = total >> 1;
        int i, move = Esquerda->numChave - leftSize, j = 0;
        for (i = Direita->numChave - 1; i >= 0; i--) {
            Direita->chave[i + move] = Direita->chave[i];
            Direita->filho[i + move] = Direita->filho[i];
            if (Direita->eFolha) Direita->pos[i + move] = Direita->pos[i];
        }
        for (i = leftSize; i < Esquerda->numChave; i++) {
            Direita->chave[j] = Esquerda->chave[i];
            Direita->filho[j] = Esquerda->filho[i];
            if (Direita->eFolha) {
                Direita->pos[j] = Esquerda->pos[i];
            } else {
                temp = (ArvoreBPlus*) Esquerda->filho[i];
                temp->pai = Direita;
            }
            j++;
        }
        Esquerda->numChave = leftSize;
        Direita->numChave = total - leftSize;
    }
}

void Deletar(ArvoreBPlus* var, int value);

void Deletar(ArvoreBPlus* Cur, int chave) {
    int i, del = Busca_Binaria(Cur, chave);
    void* delFilho = Cur->filho[del];
    for (i = del; i < Cur->numChave - 1; i++) {
        Cur->chave[i] = Cur->chave[i + 1];
        Cur->filho[i] = Cur->filho[i + 1];
        if (Cur->eFolha) Cur->pos[i] = Cur->pos[i + 1];
    }
    Cur->numChave--;
    if (Cur->eFolha == 0) {
        ArvoreBPlus* primeiroFilho = (ArvoreBPlus*) (Cur->filho[0]);
        if (primeiroFilho->eFolha == 1) {
            ArvoreBPlus* temp = (ArvoreBPlus*) delFilho;
            ArvoreBPlus* filhoAnterior = temp->anterior;
            ArvoreBPlus* filhoSucessor = temp->proximo;
            if (filhoAnterior != NULL) filhoAnterior->proximo = filhoSucessor;
            if (filhoSucessor != NULL) filhoSucessor->anterior = filhoAnterior;
        }
    }
    if (del == 0 && !Cur->eRaiz) {
        ArvoreBPlus* temp = Cur;
        while (!temp->eRaiz && temp == temp->pai->filho[0]) {
            temp->pai->chave[0] = Cur->chave[0];
            temp = temp->pai;
        }
        if (!temp->eRaiz) {
            temp = temp->pai;
            int i = Busca_Binaria(temp, chave);
            temp->chave[i] = Cur->chave[0];
        }
    }
    free(delFilho);
    if (Cur->numChave * 2 < MaxNumFilhos)
        Redistribuir(Cur);
}

ArvoreBPlus* Encontrar(int chave, int modificar) {
    ArvoreBPlus* Cur = Raiz;
    while (1) {
        if (Cur->eFolha == 1)
            break;
        if (chave < Cur->chave[0]) {
            if (modificar == 1) Cur->chave[0] = chave;
            Cur = Cur->filho[0];
        } else {
            int i = Busca_Binaria(Cur, chave);
            Cur = Cur->filho[i];
        }
    }
    return Cur;
}

void Destruir(ArvoreBPlus* Cur) {
    if (Cur->eFolha == 1) {
        int i;
        for (i = 0; i < Cur->numChave; i++)
            free(Cur->filho[i]);
    } else {
        int i;
        for (i = 0; i < Cur->numChave; i++)
            Destruir(Cur->filho[i]);
    }
    free(Cur);
}

void Print(ArvoreBPlus* Cur) {
    int i;
    for (i = 0; i < Cur->numChave; i++)
        printf("%d ", Cur->chave[i]);
    printf("\n");
    if (!Cur->eFolha) {
        for (i = 0; i < Cur->numChave; i++)
            Print(Cur->filho[i]);
    }
}

int ArvoreBPlus_Inserir(int chave, int pos, void* valor) {
    ArvoreBPlus* Folha = Encontrar(chave, 1);
    int i = Busca_Binaria(Folha, chave);
    if (Folha->chave[i] == chave) return 0;
    Inserir(Folha, chave, pos, valor);
    return 1;
}

void ArvoreBPlus_Consultar_Chave(int chave) {
    ArvoreBPlus* Folha = Encontrar(chave, 0);
    NumeroConsulta = 0;
    int i;
    for (i = 0; i < Folha->numChave; i++) {
        if (Folha->chave[i] == chave) {
            NumeroConsulta++;
            if (NumeroConsulta < 20) printf("[no.%d	chave = %d, valor = %s]\n", NumeroConsulta, Folha->chave[i], (char*) Folha->filho[i]);
        }
    }
    printf("Quantidade total de respostas: %d\n", NumeroConsulta);
}

void ArvoreBPlus_Consultar_Alcance(int l, int r) {
    ArvoreBPlus* Folha = Encontrar(l, 0);
    NumeroConsulta = 0;
    int i;
    for (i = 0; i < Folha->numChave; i++) {
        if (Folha->chave[i] >= l) break;
    }
    int finals = 0;
    while (!finals) {
        while (i < Folha->numChave) {
            if (Folha->chave[i] > r) {
                finals = 1;
                break;
            }
            NumeroConsulta++;
            if (NumeroConsulta == 20) printf("...\n");
            if (NumeroConsulta < 20) printf("[no.%d	chave = %d, valor = %s]\n", NumeroConsulta, Folha->chave[i], (char*) Folha->filho[i]);
            i++;
        }
        if (finals || Folha->proximo == NULL) break;
        Folha = Folha->proximo;
        i = 0;
    }
    printf("O número total de respostas é: %d\n", NumeroConsulta);
}

int ArvoreBPlus_Encontrar(int chave) {
    ArvoreBPlus* Folha = Encontrar(chave, 0);
    int i = Busca_Binaria(Folha, chave);
    if (Folha->chave[i] != chave) return -1;
    return Folha->pos[i];
}

void ArvoreBPlus_Modificar(int chave, void* valor) {
    ArvoreBPlus* Folha = Encontrar(chave, 0);
    int i = Busca_Binaria(Folha, chave);
    if (Folha->chave[i] != chave) return; // don't have this key
    printf("Modificar: chave = %d, valor original = %s, novo valor = %s\n", chave, (char*) (Folha->filho[i]), (char*) (valor));
    free(Folha->filho[i]);
    Folha->filho[i] = valor;
}

void ArvoreBPlus_Deletar(int chave) {
    ArvoreBPlus* Folha = Encontrar(chave, 0);
    int i = Busca_Binaria(Folha, chave);
    if (Folha->chave[i] != chave) return;
    printf("Deletar: chave = %d, valor original = %s\n", chave, (char*) (Folha->filho[i]));
    Deletar(Folha, chave);
}

void ArvoreBPlus_Destruir() {
    if (Raiz == NULL) return;
    printf("Agora destruindo Árvore B+..\n");
    Destruir(Raiz);
    Raiz = NULL;
    printf("Feito.\n");
}

void Iniciar() {
    ArvoreBPlus_Destruir();
    Raiz = Novo_NoArvoreBPlus();
    Raiz->eRaiz = 1;
    Raiz->eFolha = 1;
    TotalNos = 0;
}

void ArvoreBPlus_SetMaxFilhos(int numero) {
    MaxNumFilhos = numero + 1;
}

void ArvoreBPlus_Print() {
    struct ArvoreBPlus* Folha = Encontrar(1000000000, 0);
    int cnt = 0;
    while (Folha != NULL) {
        int i;
        for (i = Folha->numChave - 1; i >= 0; i--) {
            printf("%4d ", Folha->chave[i]);
            if (++cnt % 20 == 0) printf("\n");
        }
        Folha = Folha->anterior;
    }
}

int ArvoreBPlus_GetTotalNos() {
    return TotalNos;
}