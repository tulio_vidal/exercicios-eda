/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: tulio
 *
 * Created on 30 de Março de 2018, 22:51
 */

#include <stdio.h>
#include <stdlib.h>
#include "heap.h"

/*
 * 
 */
int main(int argc, char** argv) {
    MaxHeap heap = iniciar(10);
    int numeros[10];
    int n;
    elemento ele1;
    elemento ele2;
    
    ele1.dado = 1;
    ele2.dado = 2;
    
    printf("Informe o valor de n:");
    scanf("%d", n);
    
    for (int i = 1; i < n; i++) {
        numeros[i] = i;
        inserir(heap, (numeros[i]));
    }
    getMenor(heap, 10);
    remover(heap);
    troca(ele1.dado, ele2.dado);
    liberar(heap);
    
    return (EXIT_SUCCESS);
}

