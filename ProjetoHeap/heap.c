/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <stdlib.h>
#include "heap.h"

#define FILHOESQ(x) (2 * x + 1)
#define FILHODIR(x) (2 * x + 2)
#define PRIMO(x) ((x - 1) / 2)

MaxHeap iniciar(int tamanho) {
    MaxHeap elemento;
    elemento.tamanho = 0;
    elemento.elemento = malloc(tamanho * sizeof (elemento));
    return elemento;
}

void troca(elemento* ele1, elemento* ele2) {
    elemento temp = *ele1;
    *ele1 = *ele2;
    *ele2 = temp;
}

void construir(MaxHeap *heap, int *arr, int tamanho) {
    for (int x = 0; x < tamanho; x++) {
        if (!heap->tamanho) {
            heap->elemento = malloc(sizeof (elemento));
        } else {
            heap->elemento = realloc(heap->elemento, (heap->tamanho + 1) * sizeof (elemento));
        }
        elemento ele;
        ele.dado = arr[x];
        heap->elemento[(heap->tamanho)++] = ele;
    }

    for (int y = (heap->tamanho - 1) / 2; y >= 0; y--) {
        check(heap, y);
    }
}

void check(MaxHeap *heap, int i) {
    int maior = (FILHOESQ(i) < heap->tamanho && heap->elemento[FILHOESQ(i)].dado > heap->elemento[i].dado) ? FILHOESQ(i) : i;
    if (FILHODIR(i) < heap->tamanho && heap->elemento[FILHODIR(i)].dado > heap->elemento[maior].dado) {
        maior = FILHODIR(i);
    }
    if (maior != i) {
        troca(&(heap->elemento[i]), &(heap->elemento[maior]));
        check(heap, maior);
    }
}

void inserir(MaxHeap* heap, int elem) {
    elemento ele;
    ele.dado = elem;

    int i = heap->tamanho++;
    while (i && ele.dado > heap->elemento[PRIMO(i)].dado) {
        i = PRIMO(i);
        heap->elemento[i] = heap->elemento[PRIMO(i)];
    }
    heap->elemento[i] = ele;
}

void remover(MaxHeap *heap) {
    if (!heap->tamanho) {
        free(heap->elemento);
    } else {
        heap->elemento[0] = heap->elemento[--(heap->tamanho)];
        heap->elemento = realloc(heap->elemento, heap->tamanho * sizeof (elemento));
        check(heap, 0);
    }
}

int getMenor(MaxHeap *heap, int i) {
    if (FILHOESQ(i) >= heap->tamanho) {
        return heap->elemento[i].dado;
    }

    int esq = getMenor(heap, FILHOESQ(i));
    int dir = getMenor(heap, FILHODIR(i));

    return esq <= dir ? esq : dir;
}

void liberar(MaxHeap *heap) {
    free(heap->elemento);
}