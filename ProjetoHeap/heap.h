/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lista1.h
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 15:20
 */

typedef struct elemento {
    int dado;
} elemento;

typedef struct MaxHeap {
    elemento *elemento;
    int tamanho;
} MaxHeap;

MaxHeap* iniciar(int tamanho);
void troca(elemento* ele1, elemento* ele2);
void construir(MaxHeap *heap, int *arr, int tamanho);
void check(MaxHeap *heap, int i);
void inserir(MaxHeap *heap, int elemento);
void remover(MaxHeap *heap);
int getMenor(MaxHeap *heap, int i);
void liberar(MaxHeap* heap);