/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: tulio
 *
 * Created on 5 de Abril de 2018, 21:51
 */

#include <stdio.h>
#include <stdlib.h>
#include "conjuntos.h"

/*
 * 
 */
int main(int argc, char** argv) {
    int tamanho;

    printf("Informe o tamanho do conjunto: ");
    scanf("%d", &tamanho);

    char elementos[tamanho];
    for (int y = 0; y < tamanho; y++) {
        printf("Informe o elemento %d \n", (y + 1));
        scanf(" %c", &elementos[y]);
        printf("\nElemento inserido: %c\n", (elementos[y]));
    }

    Conjunto* conjunto = criarConjunto(elementos, tamanho);
   // conjunto = inserir(conjunto, 's');
    exibir(conjunto);
   /* Conjunto* cnj = criarConjunto();
    cnj->valor
    conjunto = uniao(conjunto->valor,cnj);
    
    */

    return (EXIT_SUCCESS);
}

