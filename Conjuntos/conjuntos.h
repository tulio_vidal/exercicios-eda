/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   conjuntos.h
 * Author: tulio
 *
 * Created on 01 de Abril de 2018, 15:20
 */

typedef struct conjunto Conjunto;

Conjunto* criarConjunto(char elementos[], int tamanho);
Conjunto* inserir(Conjunto* conjunto, char elemento);
Conjunto* remover(Conjunto* conjunto, int posicao);
char* uniao(char* conjuntoA, char* conjuntoB);
Conjunto* interseccao(Conjunto* conjuntoA, Conjunto* conjuntoB);
Conjunto* diferenca(Conjunto* conjuntoA, Conjunto* conjuntoB);
int verificarSubConjunto(Conjunto* conjuntoA, Conjunto* conjuntoB);
int verificarIgualdade(Conjunto* conjuntoA, Conjunto* conjuntoB);
int gerarComplemento(Conjunto* conjuntoA, Conjunto* conjuntoB);
int verificar(Conjunto* conjunto, char elemento);
int recuperarQuantidade(Conjunto* conjunto);
int liberar(Conjunto* conjunto);
void exibir(Conjunto* conjunto);
int obterMascara(Conjunto* conj);
int isNulo(char elemento);