/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "conjuntos.h"

struct conjunto {
    char* valor;
    int tamanho;
};

Conjunto* criarConjunto(char elementos[], int tamanho) {

    if (tamanho == 0) {
        return NULL;
    }

    Conjunto* conjunto = (Conjunto*) malloc(sizeof (Conjunto));
    int comp = (tamanho * sizeof (conjunto->valor)) / sizeof (char*);
    conjunto->valor = malloc(comp);
    conjunto->tamanho = tamanho;

    for (int x = 0; x < tamanho; x++) {
        conjunto->valor[x] = elementos[x];
    }
    int mascara = obterMascara(conjunto);
    printf("Tamanho: %d", mascara);

    return conjunto;
}

Conjunto* inserir(Conjunto* conjunto, char elemento) {
    int pos = conjunto->tamanho;
    conjunto->valor[pos] = elemento;
    return conjunto;
}

Conjunto* remover(Conjunto* conjunto, int posicao) {
    conjunto->valor[posicao] = ' ';
    return conjunto;
}

Conjunto* uniao(Conjunto* conjuntoA, Conjunto* conjuntoB) {
    Conjunto* conjuntoC = (Conjunto*) malloc(sizeof (conjuntoA));
    
    int mascara1 = obterMascara(conjuntoA);
    int mascara2 = obterMascara(conjuntoB);
    int mascara3 = mascara1 | mascara2;
    
    for (int x = 0; x < conjuntoA->tamanho && x < conjuntoB->tamanho; x++) {
        if (conjuntoA->valor[x] == conjuntoB->valor[x]) {
            conjuntoC->valor[x] = conjuntoB->valor[x];
        }
    }
    return conjuntoC;
}

void exibir(Conjunto* conjunto) {
    for (int x = 0; x < strlen(conjunto->valor) - 1; x++) {
        if (isNulo(conjunto->valor[x])) {
            printf("valor %c \n", conjunto->valor[x]);
        }
    }
}

int isNulo(char elemento) {
    return isalpha(elemento);
}

int obterMascara(Conjunto* conj) {
    int mascaraBits = 0;
    for (int i = 0; i < conj->tamanho; i++) {
        if (isNulo(conj->valor[i])) {
            mascaraBits = 1 << i; //0001
        }
    }
    return mascaraBits;
}