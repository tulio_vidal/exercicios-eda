/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdlib.h>
#include <stdio.h>
#include "lista5.h"

/**
 * @struct listaCircularDuplamenteEncadeada
 * Lista Duplamente Encadeada
 * { 
 *  Proximo, 
 *  Elemento
 * }
 */
struct listaCircularDuplamenteEncadeada {
    ListaCircularDuplamenteEncadeada* proximo;
    ListaCircularDuplamenteEncadeada* anterior;
    int elemento;
};

typedef struct listaCircularDuplamenteEncadeada ListaCircularDuplamenteEncadeada;

ListaCircularDuplamenteEncadeada* criar() {
    return NULL;
}

ListaCircularDuplamenteEncadeada* inserir(ListaCircularDuplamenteEncadeada* lista, int elemento) {
    ListaCircularDuplamenteEncadeada* listaEnc = (ListaCircularDuplamenteEncadeada*)
            malloc(sizeof (ListaCircularDuplamenteEncadeada));
    listaEnc->elemento = elemento;
    
     if (verificar(lista)) {
        listaEnc->anterior = listaEnc;
        listaEnc->proximo = listaEnc;
        return listaEnc;
    }

    listaEnc->proximo = lista;
    listaEnc->anterior = lista->anterior;
    lista->anterior->proximo = listaEnc;
    lista->anterior = listaEnc;
    lista = listaEnc;
    
    return lista;
}

void imprimir(ListaCircularDuplamenteEncadeada* lista) {
    if (verificar(lista)) {
        return;
    }
    ListaCircularDuplamenteEncadeada* proximo = lista;
    do {
        printf("%d \n", proximo->elemento);
        proximo = proximo->proximo;
    } while (proximo != NULL);
}

void imprimirRecursao(ListaCircularDuplamenteEncadeada* lista) {
    if (!verificar(lista)) {
        printf("valor: %d\n", lista->elemento);
        imprimirRecursao(lista->proximo);
    }
}

int verificar(ListaCircularDuplamenteEncadeada* lista) {
    return lista == NULL;
}

int retornarTamanho(ListaCircularDuplamenteEncadeada* lista) {
    int x = 0;

    for (ListaCircularDuplamenteEncadeada* temp = lista; temp != NULL; temp = temp->proximo) {
        x++;
    }
    return x;
}

ListaCircularDuplamenteEncadeada* buscar(ListaCircularDuplamenteEncadeada* lista, int elemento) {
    for (ListaCircularDuplamenteEncadeada* proximo = lista; proximo != NULL; proximo = proximo->proximo) {
        if (proximo->elemento == elemento)
            return proximo;
    }
    return NULL;
}

ListaCircularDuplamenteEncadeada* remover(ListaCircularDuplamenteEncadeada* lista, int elemento) {
    if (buscar(lista, elemento) == NULL) {
        return lista;
    }

    ListaCircularDuplamenteEncadeada* proximo = buscar(lista, elemento);

    if (lista != proximo) {
        proximo->anterior->proximo = proximo->proximo;
    } else {
        lista = proximo->proximo;
    }

    if (proximo->proximo != NULL) {
        proximo->proximo->anterior = proximo->anterior;
    }

    free(proximo);
    return lista;
}

ListaCircularDuplamenteEncadeada* removerRecursao(ListaCircularDuplamenteEncadeada* lista, int elemento) {
    if (!verificar(lista)) {
        if (lista->elemento != elemento) {
            lista->proximo = removerRecursao(lista->proximo, elemento);
        } else {
            ListaCircularDuplamenteEncadeada* aux = lista;
            lista = lista->proximo;
            free(aux);
        }
    }
    return lista;
}

void liberar(ListaCircularDuplamenteEncadeada* lista) {
    ListaCircularDuplamenteEncadeada* temp = lista;
    while (temp != NULL) {
        ListaCircularDuplamenteEncadeada* aux = temp->proximo;
        free(temp);
        temp = aux;
    }
}