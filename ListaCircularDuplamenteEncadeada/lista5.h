/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lista5.h
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 15:20
 */

typedef struct listaCircularDuplamenteEncadeada ListaCircularDuplamenteEncadeada;

ListaCircularDuplamenteEncadeada* criar(void);
ListaCircularDuplamenteEncadeada* inserir(ListaCircularDuplamenteEncadeada* lista, int elemento);
void imprimir(ListaCircularDuplamenteEncadeada* lista);
void imprimirRecursao(ListaCircularDuplamenteEncadeada* lista);
int verificar(ListaCircularDuplamenteEncadeada* lista);
int retornarTamanho(ListaCircularDuplamenteEncadeada* lista);
ListaCircularDuplamenteEncadeada* buscar(ListaCircularDuplamenteEncadeada* lista, int elemento);
ListaCircularDuplamenteEncadeada* remover(ListaCircularDuplamenteEncadeada* lista, int elemento);
ListaCircularDuplamenteEncadeada* removerRecursao(ListaCircularDuplamenteEncadeada* lista, int elemento);
void liberar(ListaCircularDuplamenteEncadeada* lista);