/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdlib.h>
#include <stdio.h>
#include "lista2.h"

/**
 * @struct listaEncadeada
 * Lista Encadeada
 * { 
 *  Proximo, 
 *  Elemento
 * }
 */
struct listaEncadeada {
    ListaEncadeada* proximo;
    int elemento;
};

typedef struct listaEncadeada ListaEncadeada;

ListaEncadeada* criar() {
    return NULL;
}

ListaEncadeada* inserir(ListaEncadeada* lista, int elemento) {
    ListaEncadeada* novo = (ListaEncadeada*) malloc(sizeof (ListaEncadeada));
    ListaEncadeada* anterior = NULL;
    ListaEncadeada* proximo = lista;

    novo->elemento = elemento;

    while (proximo != NULL && proximo->elemento < elemento) {
        anterior = proximo;
        proximo = proximo->proximo;
    }

    novo->proximo = ((anterior != NULL) ? anterior->proximo : lista);

    if (anterior != NULL) {
        anterior->proximo = novo;
    } else {
        lista = novo;
    }
    return lista;
}

void imprimir(ListaEncadeada* lista) {
    for (ListaEncadeada* temp = lista; temp != NULL; temp = temp->proximo) {
        printf("valores: %d \n", temp->elemento);
    }
}

void imprimirReversa(ListaEncadeada* lista) {
    int x = 0;

    ListaEncadeada * listaAux[retornarTamanho(lista)];
    for (ListaEncadeada* temp = lista; temp != NULL; temp = temp->proximo, x++) {
        listaAux[x] = temp;
    }

    for (int z = x - 1; z >= 0; z--) {
        printf("valores inversos: %d \n", listaAux[z]->elemento);
    }
}

void imprimirRecursao(ListaEncadeada* lista) {
    if (!verificar(lista)) {
        printf("valor: %d\n", lista->elemento);
        imprimirRecursao(lista->proximo);
    }
}

int verificar(ListaEncadeada* lista) {
    return lista == NULL;
}

int verificarIgualdade(ListaEncadeada* list1, ListaEncadeada* list2) {
    ListaEncadeada* lista1 = list1;
    ListaEncadeada* lista2 = list2;
    for (; lista1 != NULL && lista2 != NULL; lista1 = lista1->proximo,
            lista2 = lista2->proximo) {
        if (lista1->elemento != lista2->elemento)
            return 0;
    }
    return (lista1 == lista2);
}

int retornarTamanho(ListaEncadeada* lista) {
    int x = 0;

    for (ListaEncadeada* temp = lista; temp != NULL; temp = temp->proximo) {
        x++;
    }
    return x;
}

ListaEncadeada* buscar(ListaEncadeada* lista, int elemento) {
    for (ListaEncadeada* proximo = lista; proximo != NULL; proximo = proximo->proximo) {
        if (proximo->elemento == elemento)
            return proximo;
    }
    return NULL;
}

ListaEncadeada* remover(ListaEncadeada* lista, int elemento) {
    if (verificar(lista->proximo)) {
        return lista;
    }

    ListaEncadeada* anterior = NULL;
    ListaEncadeada* prox = lista;

    while ((prox != NULL) && (prox->elemento != elemento)) {
        anterior = prox;
        prox = prox->proximo;
    }

    if (verificar(anterior)) {
        lista = prox->proximo;
    } else {
        anterior->proximo = prox->proximo;
    }

    free(prox);
    return lista;
}

ListaEncadeada* removerRecursao(ListaEncadeada* lista, int elemento) {
    if (!verificar(lista)) {
        if (lista->elemento != elemento) {
            lista->proximo = removerRecursao(lista->proximo, elemento);
        } else {
            ListaEncadeada* aux = lista;
            lista = lista->proximo;
            free(aux);
        }
    }
    return lista;
}

void liberar(ListaEncadeada* lista) {
    ListaEncadeada* temp = lista;
    while (temp != NULL) {
        ListaEncadeada* aux = temp->proximo;
        free(temp);
        temp = aux;
    }
}