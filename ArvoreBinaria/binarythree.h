/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   binarythree.h
 * Author: tulio
 *
 * Created on 4 de Maio de 2018, 22:32
 */


typedef struct No
{
    int dado;
    struct No* esquerda;
    struct No* direita;
} No;

typedef int (*comparer)(int, int);
typedef void (*callback)(No*);

No* criar_No(int dado);
No* inserir_No(No *raiz, comparer compare, int dado);
No* deletar_No(No * raiz, int dado, comparer compare);
No* buscar(No *raiz,const int dado,comparer compare);
void traverse(No *raiz,callback cb);
void dispose(No * raiz);
int comparar(int esquerda,int direita);
void exibir(No* nd);
void exibir_arvore(No* nd);
