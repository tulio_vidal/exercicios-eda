/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: tulio
 *
 * Created on 4 de Maio de 2018, 22:28
 */

#include <stdio.h>
#include <stdlib.h>
#include "binarythree.h"

/*
 * 
 */
int main(int argc, char** argv) {
No* raiz = NULL;
    comparer int_comp = comparar;
    callback f = exibir;
 
    /* insert dado into the tree */
    int a[9] = {8,3,10,1,6,14,4,7,13};
    int i;
    printf("--- C Árvore Binária de Busca ---- \n\n");
    printf("Inserindo: ");
    for(i = 0; i < 9; i++)
    {
        printf("%d ",a[i]);
        raiz = inserir_No(raiz,int_comp,a[i]);
    }
    printf(" dentro da árvore.\n\n");
 
    /* display the tree */
    exibir_arvore(raiz);
 
    /* remove element */
    int r;
    do
    {
        printf("Insira o dado para remover, (-1 para sair):");
        scanf("%d",&r);
        if(r == -1)
            break;
        raiz = deletar_No(raiz,r,int_comp);
        if(raiz != NULL)
            exibir_arvore(raiz);
        else
            break;
    }
    while(raiz != NULL);

    int key = 0;
    No *s;
    while(key != -1)
    {
        printf("Insira o dado para buscar (-1 para sair):");
        scanf("%d",&key);
 
        s = buscar(raiz,key,int_comp);
        if(s != NULL)
        {
            printf("Encontrado %d",s->dado);
            if(s->esquerda != NULL)
                printf("(L: %d)",s->esquerda->dado);
            if(s->direita != NULL)
                printf("(R: %d)",s->direita->dado);
            printf("\n");
        }
        else
        {
            printf("No %d não encontrado\n",key);
        }
    }
 
    dispose(raiz);
    return 0;
}

