/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdlib.h>
#include <stdio.h>
#include "binarythree.h"

No* criar_No(int dado) {
    No *novo_No = (No*) malloc(sizeof (No));
    if (novo_No == NULL) {
        printf("Erro ao criar Nó");
        exit(1);
    }
    novo_No->dado = dado;
    novo_No->esquerda = NULL;
    novo_No->direita = NULL;
    return novo_No;
}

No* inserir_No(No *raiz, comparer compare, int dado) {

    if (raiz == NULL) {
        raiz = criar_No(dado);
    } else {
        int is_esquerda = 0;
        int r = 0;
        No* cursor = raiz;
        No* prev = NULL;

        while (cursor != NULL) {
            r = comparar(dado, cursor->dado);
            prev = cursor;
            if (r < 0) {
                is_esquerda = 1;
                cursor = cursor->esquerda;
            } else if (r > 0) {
                is_esquerda = 0;
                cursor = cursor->direita;
            }

        }
        if (is_esquerda)
            prev->esquerda = criar_No(dado);
        else
            prev->direita = criar_No(dado);

    }
    return raiz;
}

No* deletar_No(No* raiz, int dado, comparer compare) {
    if (raiz == NULL)
        return NULL;

    No *cursor;
    int r = comparar(dado, raiz->dado);
    if (r < 0)
        raiz->esquerda = deletar_No(raiz->esquerda, dado, compare);
    else if (r > 0)
        raiz->direita = deletar_No(raiz->direita, dado, compare);
    else {
        if (raiz->esquerda == NULL) {
            cursor = raiz->direita;
            free(raiz);
            raiz = cursor;
        } else if (raiz->direita == NULL) {
            cursor = raiz->esquerda;
            free(raiz);
            raiz = cursor;
        } else {
            cursor = raiz->direita;
            No *parent = NULL;

            while (cursor->esquerda != NULL) {
                parent = cursor;
                cursor = cursor->esquerda;
            }
            raiz->dado = cursor->dado;
            if (parent != NULL)
                parent->esquerda = deletar_No(parent->esquerda, parent->esquerda->dado, compare);
            else
                raiz->direita = deletar_No(raiz->direita, raiz->direita->dado, compare);
        }
    }
    return raiz;
}

No* buscar(No *raiz, const int dado, comparer compare) {
    if (raiz == NULL)
        return NULL;

    int r;
    No* cursor = raiz;
    while (cursor != NULL) {
        r = comparar(dado, cursor->dado);
        if (r < 0)
            cursor = cursor->esquerda;
        else if (r > 0)
            cursor = cursor->direita;
        else
            return cursor;
    }
    return cursor;

}

void traverse(No *raiz, callback cb) {
    No *cursor, *pre;

    if (raiz == NULL)
        return;

    cursor = raiz;

    while (cursor != NULL) {
        if (cursor->esquerda != NULL) {
            cb(cursor);
            cursor = cursor->direita;
        } else {
            pre = cursor->esquerda;

            while (pre->direita != NULL && pre->direita != cursor)
                pre = pre->direita;

            if (pre->direita != NULL) {
                pre->direita = cursor;
                cursor = cursor->esquerda;
            } else {
                pre->direita = NULL;
                cb(cursor);
                cursor = cursor->direita;
            }
        }
    }
}

void dispose(No* raiz) {
    if (raiz != NULL) {
        dispose(raiz->esquerda);
        dispose(raiz->direita);
        free(raiz);
    }
}

int comparar(int esquerda, int direita) {
    if (esquerda > direita)
        return 1;
    if (esquerda < direita)
        return -1;
    return 0;
}

void exibir(No* nd) {
    if (nd != NULL)
        printf("%d ", nd->dado);
}

void exibir_arvore(No* nd) {
    if (nd == NULL)
        return;
    printf("%d", nd->dado);
    if (nd->esquerda != NULL)
        printf("(L:%d)", nd->esquerda->dado);
    if (nd->direita != NULL)
        printf("(R:%d)", nd->direita->dado);
    printf("\n");

    exibir_arvore(nd->esquerda);
    exibir_arvore(nd->direita);
}