/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 14:28
 */

/**
 * Inclusões
 */
#include <stdio.h>
#include <stdlib.h>
#include "lista1.h"

/**
 * Método Principal
 * @return 
 */
int main(void) {
    ListaEncadeada* lista = criar();
    lista = inserir(lista, 1);
    lista = inserir(lista, 2);
    lista = inserir(lista, 3);
    imprimir(lista);
    imprimirRecursao(lista);
    imprimirReversa(lista);
    verificar(lista);
    remover(lista, 2);
    lista = remover(lista, 1);
    lista = removerRecursao(lista, 3);
    lista = buscar(lista, 2);
    imprimir(lista);
    return 0;
}


