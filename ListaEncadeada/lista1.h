/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   lista1.h
 * Author: tulio
 *
 * Created on 11 de Março de 2018, 15:20
 */

typedef struct listaEncadeada ListaEncadeada;

ListaEncadeada* criar(void);
ListaEncadeada* inserir(ListaEncadeada* lista, int elemento);
void imprimir(ListaEncadeada* lista);
void imprimirRecursao(ListaEncadeada* lista);
void imprimirReversa(ListaEncadeada* lista);
int verificar(ListaEncadeada* lista);
int retornarTamanho(ListaEncadeada* lista);
ListaEncadeada* buscar(ListaEncadeada* lista, int elemento);
ListaEncadeada* remover(ListaEncadeada* lista, int elemento);
ListaEncadeada* removerRecursao(ListaEncadeada* lista, int elemento);
void liberar(ListaEncadeada* lista);