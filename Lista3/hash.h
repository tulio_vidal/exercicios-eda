/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   hash.h
 * Author: tulio
 *
 * Created on 19 de Março de 2018, 18:58
 */

typedef struct hash Hash;

int funcao(int chave, int tamanho);
void criarHash(Hash* hashes[], int tamanho);
void insereHash(Hash* hashes[], int chave, int valor, int tamanho);
Hash* buscaHash(Hash* hashes[], int chave, int tamanho);
void imprimeHash(Hash* hashes[], int tamanho);
Hash* removeHash(Hash* item, Hash* itemVazio, Hash* hashes[], int tamanho);
void liberar(Hash* hashes[], int tamanho);