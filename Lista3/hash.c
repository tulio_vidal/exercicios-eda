/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   hash.c
 * Author: tulio
 *
 * Created on 19 de Março de 2018, 18:58
 */

#include <stdio.h>
#include <stdlib.h>
#include "hash.h"

struct hash {
    int chave;
    int elemento;
};

typedef struct hash Hash;

int funcao(int chave, int tamanho) {
    return (chave % tamanho);
}


void criarHash(Hash* hashes[], int tamanho) {
    for (int i = 0; i < tamanho; i++) {
        hashes[i] = NULL;
    }
}

void insereHash(Hash* hashes[], int chave, int valor, int tamanho) {
    Hash* temp = (Hash*) malloc(sizeof (Hash));
    temp->elemento = valor;
    temp->chave = chave;

    int hashIndex = funcao(chave, tamanho);

    while (hashes[hashIndex] != NULL && hashes[hashIndex]->chave != -1) {
        ++hashIndex;

        hashIndex %= tamanho;
    }

    hashes[hashIndex] = temp;
}

Hash* buscaHash(Hash* hashes[], int chave, int tamanho) {
    int hashIndex = funcao(chave, tamanho);

    while (hashes[hashIndex] != NULL) {

        if (hashes[hashIndex]->chave == chave)
            return hashes[hashIndex];

        ++hashIndex;

        hashIndex %= tamanho;
    }
    return NULL;
}

Hash* removeHash(Hash* hash, Hash* itemvazio, Hash* hashes[], int tamanho) {
    int chave = hash->chave;

    int hashIndex = funcao(chave, tamanho);

    while (hashes[hashIndex] != NULL) {

        if (hashes[hashIndex]->chave == chave) {
            Hash* temp = hashes[hashIndex];

            hashes[hashIndex] = itemvazio;
            return temp;
        }

        ++hashIndex;

        hashIndex %= tamanho;
    }

    return NULL;
}

void imprimeHash(Hash* hashes[], int tamanho) {
    int i = 0;

    for (i = 0; i < tamanho; i++) {

        if (hashes[i] != NULL)
            printf(" (%d,%d)", hashes[i]->chave, hashes[i]->elemento);
        else
            printf(" -- ");
    }

    printf("\n");
}

int main(int argc, char** argv) {
    int elemento, n, tamanho;
    printf("Informe N:");
    scanf("%d", &n);

    tamanho = n / 2;

    Hash* itemVazio = (Hash*) malloc(sizeof (Hash));
    itemVazio->elemento = -1;
    itemVazio->chave = -1;

    Hash * hashes[tamanho];
    criarHash(hashes, tamanho);

    insereHash(hashes, 1, 10, tamanho);
    insereHash(hashes, 2, 20, tamanho);
    insereHash(hashes, 60, 17, tamanho);
    insereHash(hashes, 4, 35, tamanho);

    imprimeHash(hashes, tamanho);

    Hash* itemEncontrado = buscaHash(hashes, 1, tamanho);

    if (itemEncontrado != NULL) {
        printf("Elemento encontrado!\n");
    } else {
        printf("Elemento inexistente no Hash\n");
    }

    removeHash(itemEncontrado, itemVazio, hashes, tamanho);

    itemEncontrado = buscaHash(hashes, 1, tamanho);
    
    if (itemEncontrado != NULL) {
        printf("Elemento encontrado!\n");
    } else {
        printf("Elemento inexistente no Hash\n");
    }
    
    liberar(hashes, tamanho);

    return 0;
}

void liberar(Hash* hashes[], int tamanho) {
    for(int i = 0; i < tamanho; i++) {
        free(hashes[i]);
    }
}